#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "utilfecha.h"

int main(int argc, char **argv){
	int seg;
	int days;
	char opcion;
	while((opcion = getopt(argc, argv, "s:d:f:")) != -1 ){
		switch(opcion){
			case 's':
				seg = atoi(optarg);
				segundos(seg);
				break;
			case 'd':
				days = atoi(optarg);
				dias(days);
				break;
			case 'f':
				formato(optarg);
				break;
			default:
			       break;	
		}
	}
	return 0;
}

