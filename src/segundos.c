#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void segundos(int seg){
	int resto;
        int horas = seg/3600;
	resto = seg%3600;
	int minutos= resto/60;
	int segundos = resto%60;
        printf("Horas \tMinutos \tSegundos\n");
	printf("%d %8d %15d\n", horas,minutos,segundos);
}
