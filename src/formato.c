#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

char* mes(char* meses);

void formato(char *entrada){
	char anio [5];
	char meses[3];
	char dias[3];
	char hora[9];
	char formato[30];
	strncpy(anio,entrada,4);
	anio[4]='\0';

	strncpy(dias,entrada+8,2);
	dias[2]='\0';

	strncpy(meses,entrada+5,2);
	meses[2]='\0';

	strcpy(hora,strrchr(entrada,' ')+1);
	hora[9]='\0';

	strcat(formato,dias);
	strcat(formato," de ");
	strcat(formato,mes(meses));
	strcat(formato," del ");
	strcat(formato,anio);
	strcat(formato,", ");
	strcat(formato,hora);
	printf("%s\n", formato);
}

char* mes(char* meses){
	int month = atoi(meses);
	switch(month){
		case 1:
			return "Enero";
			break;
		case 2:
			return "Febrero";
			break;
		case 3:
			return "Marzo";
			break;
		case 4:
			return "Abril";
			break;
		case 5:
			return "Mayo";
			break;
		case 6:
			return "Junio";
			break;
		case 7:
			return "Julio";
			break;
		case 8:
			return "Agosto";
			break;
		case 9:
			return "Septiembre";
			break;
		case 10:
			return "Octubre";
			break;
		case 11:
			return "Noviembre";
			break;
		case 12:
			return "Diciembre";
			break;
		default:
			break;
	}
	
	return NULL;
}
