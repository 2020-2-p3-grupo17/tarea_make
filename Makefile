CC=gcc -Wall -c
IC=-I include/
bin/tarea:  obj/segundos.o obj/dias.o obj/formato.o obj/main.o 
	   mkdir -p bin/
	   gcc $^ -o bin/utilfecha

obj/main.o: src/main.c
		mkdir -p obj/
		$(CC) -I include/ src/main.c -o obj/main.o

obj/segundos.o: src/segundos.c
		$(CC) $(IC) src/segundos.c -o obj/segundos.o

obj/dias.o: src/dias.c
	    $(CC)  src/dias.c -o obj/dias.o

obj/formato.o: src/formato.c
		$(CC)  src/formato.c -o obj/formato.o
.Phony: clean
clean: 
	rm obj/* bin/*	
